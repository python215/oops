# python oops concepts
Includes the following concepts
` 
Classes
Inheritance
Methods
Dunder
Polymorphism
Encapsulation

`
# References
[**GeeksForGeeks**](https://www.geeksforgeeks.org/python-oops-concepts/)
