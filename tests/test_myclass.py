import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../src')

from basic_class import MyClass

def test_myclass():
    myObj = MyClass("Gopi", 36)
    print(f"Name: {myObj.getName()}")
    print(f"Age:  {myObj.getAge()}")
    assert myObj.getName() == "Gopi"
    assert myObj.getAge() == 36

